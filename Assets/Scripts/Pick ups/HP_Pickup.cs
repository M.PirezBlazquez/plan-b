using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class HP_Pickup : NetworkBehaviour
{
    public int healAmount = 2;
    private bool isPlayerInRange = false;
    private PlayerHP playerHP;

    private void Update()
    {
        if (isPlayerInRange && playerHP != null && Input.GetKeyDown(KeyCode.E))
        {
            if (playerHP.currentHealth.Value < playerHP.numHearts * 2)
            {
                HealPlayerServerRpc(playerHP.OwnerClientId, healAmount);
            }
            else
            {
                Debug.Log("Vida Completa");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = true;
            playerHP = other.gameObject.GetComponent<PlayerHP>();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isPlayerInRange = false;
            playerHP = null;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void HealPlayerServerRpc(ulong playerId, int amount)
    {
        NetworkManager.Singleton.ConnectedClients[playerId].PlayerObject.GetComponent<PlayerHP>().Heal(amount);
        DestroyPickupClientRpc();
    }

    [ClientRpc]
    private void DestroyPickupClientRpc()
    {
        Destroy(gameObject);
    }
}
