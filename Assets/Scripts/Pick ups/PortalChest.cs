using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalChest : MonoBehaviour
{
    public int maxCrystals = 50;
    public float collectionInterval = 0.1f;
    public Collider2D collectionArea;
    public GameObject[] rewardPrefabs;

    private int currentCrystals = 0;
    private bool isCollecting = false;
    private CrystalInventory playerInventory;

    private void Start()
    {
        collectionArea.enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerInventory = collision.GetComponent<CrystalInventory>();
            if (playerInventory != null && !isCollecting)
            {
                StartCoroutine(CollectCrystals());
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StopCoroutine(CollectCrystals());
            isCollecting = false;
        }
    }

    private IEnumerator CollectCrystals()
    {
        isCollecting = true;
        while (currentCrystals < maxCrystals && playerInventory.GetCurrentCrystals() > 0)
        {
            playerInventory.UseCrystal();
            currentCrystals++;
            yield return new WaitForSeconds(collectionInterval);

            if (currentCrystals >= maxCrystals)
            {
                OpenChest();
                break;
            }
        }

        isCollecting = false;
    }

    private void OpenChest()
    {
        int rewardIndex = Random.Range(0, rewardPrefabs.Length);
        Instantiate(rewardPrefabs[rewardIndex], transform.position, Quaternion.identity);

        Destroy(gameObject);
    }
}