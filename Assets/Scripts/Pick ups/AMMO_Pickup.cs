using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AMMO_Pickup : MonoBehaviour
{
    public int refillType; // 1 = 100%, 2 = 50%, 3 f= 25%
    private bool isPlayerInRange = false;
    private AdditionalGun playerGun;

    void Update()
    {
        if (isPlayerInRange && Input.GetKeyDown(KeyCode.E))
        {
            if (playerGun != null && playerGun.currentAmmo < playerGun.maxAmmo)
            {
                playerGun.RefillAMMO(refillType);
                Destroy(gameObject);
            }
            else
            {
                Debug.Log("FULL AMMO");
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerInRange = true;
            playerGun = other.GetComponentInChildren<AdditionalGun>();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isPlayerInRange = false;
            playerGun = null;
        }
    }
}
