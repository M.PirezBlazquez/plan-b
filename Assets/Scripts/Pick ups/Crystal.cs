using UnityEngine;
using Unity.Netcode;

public class Crystal : NetworkBehaviour
{
    public float minSpeed = 1f;
    public float maxSpeed = 5f;
    public float accelerationTime = 2f;

    private Transform targetPlayer;
    private bool isCollected = false;
    private float speed;
    private float elapsedTime = 0f;

    private void Start()
    {
        speed = minSpeed;
    }

    private void OnEnable()
    {
        isCollected = false;
        elapsedTime = 0f;
    }

    private void Update()
    {
        if (isCollected)
        {
            elapsedTime += Time.deltaTime;
            speed = Mathf.Lerp(minSpeed, maxSpeed, elapsedTime / accelerationTime);

            if (targetPlayer != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPlayer.position, speed * Time.deltaTime);

                if (Vector3.Distance(transform.position, targetPlayer.position) < 0.1f)
                {
                    if (IsServer)
                    {
                        CollectCrystal(targetPlayer.GetComponent<NetworkObject>().OwnerClientId);
                    }
                    gameObject.SetActive(false);
                }
            }
            else
            {
                speed = 0f;
                isCollected = false;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerCollectArea"))
        {
            if (!isCollected)
            {
                targetPlayer = collision.transform.parent;
                isCollected = true;
                SetCrystalOwnerServerRpc(NetworkManager.Singleton.LocalClientId);
            }
        }
    }

    [ServerRpc(RequireOwnership = false)]
    private void SetCrystalOwnerServerRpc(ulong playerId)
    {
        GetComponent<NetworkObject>().ChangeOwnership(playerId);
    }

    private void CollectCrystal(ulong playerId)
    {
        CollectCrystalClientRpc(playerId);
    }

    [ClientRpc]
    private void CollectCrystalClientRpc(ulong playerId)
    {
        if (NetworkManager.Singleton.LocalClientId == playerId)
        {
            CrystalInventory playerInventory = NetworkManager.Singleton.SpawnManager.GetPlayerNetworkObject(playerId).GetComponent<CrystalInventory>();
            playerInventory.CollectCrystal();
        }
        gameObject.SetActive(false);
    }
}
