using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CrystalInventory : MonoBehaviour
{
    public int maxCrystals = 10;
    [SerializeField] private int currentCrystals = 0;
    public GameObject collectionArea;
    public TMP_Text crystalText;

    private void Start()
    {
        UpdateCrystalUI();
    }

    private void Update()
    {
        if (collectionArea != null)
        {
            if (currentCrystals >= maxCrystals)
            {
                collectionArea.SetActive(false);
                if (crystalText != null)
                {
                    crystalText.color = Color.red;
                }
            }
            else
            {
                collectionArea.SetActive(true);
                if (crystalText != null)
                {
                    crystalText.color = Color.white;
                }
            }
        }

        UpdateCrystalUI();
    }

    public bool CanCollectMoreCrystals()
    {
        return currentCrystals < maxCrystals;
    }

    public void CollectCrystal()
    {
        if (currentCrystals < maxCrystals)
        {
            currentCrystals++;
            UpdateCrystalUI();
        }
    }

    public void UseCrystal()
    {
        if (currentCrystals > 0)
        {
            currentCrystals--;
            UpdateCrystalUI();
        }
    }

    public int GetCurrentCrystals()
    {
        return currentCrystals;
    }

    private void UpdateCrystalUI()
    {
        if (crystalText != null)
        {
            crystalText.text = "" + currentCrystals;
        }
    }
}
