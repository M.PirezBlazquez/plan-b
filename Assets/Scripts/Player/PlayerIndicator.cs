using UnityEngine;
using Unity.Netcode;

public class PlayerIndicator : NetworkBehaviour
{
    [Header("Indicator Object")]
    public GameObject indicatorObject;

    public override void OnNetworkSpawn()
    {
        if (IsOwner)
        {
            indicatorObject.SetActive(true);
        }
        else
        {
            indicatorObject.SetActive(false);
        }
    }
}