using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInventory : MonoBehaviour
{
    public Transform baseReference;
    public Transform additionalReference;   
    private GameObject currentAdditionalWeapon;

    void Start()
    {
        if (baseReference.childCount > 0)
        {
            baseReference.GetChild(0).gameObject.SetActive(true);
        }

        if (additionalReference.childCount > 0)
        {
            currentAdditionalWeapon = additionalReference.GetChild(0).gameObject;
            currentAdditionalWeapon.SetActive(false);
        }
    }

    public void EquipWeapon(GameObject newWeapon)
    {
        if (currentAdditionalWeapon != null)
        {
            Destroy(currentAdditionalWeapon);
        }
        newWeapon.transform.SetParent(additionalReference);
        newWeapon.transform.localPosition = Vector3.zero;
        newWeapon.transform.localRotation = Quaternion.identity;
        newWeapon.SetActive(true);

        currentAdditionalWeapon = newWeapon;
        currentAdditionalWeapon.SetActive(true);
    }

    public void SwitchWeapon()
    {
        if (currentAdditionalWeapon != null)
        {
            bool isBaseWeaponActive = baseReference.GetChild(0).gameObject.activeSelf;
            baseReference.GetChild(0).gameObject.SetActive(!isBaseWeaponActive);
            currentAdditionalWeapon.SetActive(isBaseWeaponActive);
        }
    }
}
