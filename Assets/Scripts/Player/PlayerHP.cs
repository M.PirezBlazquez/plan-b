using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using Unity.Netcode;

public class PlayerHP : NetworkBehaviour
{
    public int MaxHealth;
    public NetworkVariable<int> currentHealth = new NetworkVariable<int>();
    public int numHearts;
    public float invulnerabilityDuration = 0.8f; 

    public Image[] hearts;
    public Sprite Full;
    public Sprite Half;
    public Sprite Empty;

    public Collider2D damageCollider;

    private bool isInvulnerable = false;

    public override void OnNetworkSpawn()
    {
        if (IsServer)
        {
            currentHealth.Value = MaxHealth;
        }
        UpdateHearts();
    }

    private void Update()
    {
        if (IsOwner)
        {
            UpdateHearts();
        }
    }

    public void TakeDamage(int damage)
    {
        if (!isInvulnerable)
        {
            currentHealth.Value -= damage;
            if (currentHealth.Value < 0)
            {
                currentHealth.Value = 0;
            }
            UpdateHearts();
            StartCoroutine(InvulnerabilityCoroutine(invulnerabilityDuration));
        }
    }

    private void UpdateHearts()
    {
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < currentHealth.Value / 2)
            {
                hearts[i].sprite = Full;
            }
            else if (i == Mathf.FloorToInt(currentHealth.Value / 2f) && currentHealth.Value % 2 != 0)
            {
                hearts[i].sprite = Half;
            }
            else
            {
                hearts[i].sprite = Empty;
            }

            hearts[i].enabled = i < numHearts;
        }
    }

    public void Heal(int healAmount)
    {
        currentHealth.Value += healAmount;
        if (currentHealth.Value > numHearts * 2)
        {
            currentHealth.Value = numHearts * 2;
        }
        UpdateHearts();
    }

    private IEnumerator InvulnerabilityCoroutine(float duration)
    {
        SetInvulnerability(true);
        yield return new WaitForSeconds(duration);
        SetInvulnerability(false);
    }

    private void SetInvulnerability(bool state)
    {
        isInvulnerable = state;
        damageCollider.enabled = !state;  // Desactivar el collider si es invulnerable, activarlo si no lo es
    }
}
