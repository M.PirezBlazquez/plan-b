using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class AdditionalMagazineUI : NetworkBehaviour
{
    public Slider magazineSlider;
    public GameObject additionalReference;
    private AdditionalGun additionalGun;

    void Update()
    {
        if (!IsOwner) return;

        if (additionalGun != null)
        {
            magazineSlider.maxValue = additionalGun.magazineSize;
            magazineSlider.value = additionalGun.currentMagazine;
        }
        else
        {
            magazineSlider.value = 0;
        }
        UpdateMagazineUI();
    }

    public void SetAdditionalGun(AdditionalGun gun)
    {
        additionalGun = gun;
    }

    void UpdateMagazineUI()
    {
        if (!IsOwner) return;

        AdditionalGun currentGun = additionalReference.GetComponentInChildren<AdditionalGun>();
        SetAdditionalGun(currentGun);
    }
}
