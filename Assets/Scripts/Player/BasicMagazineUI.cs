using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class BasicMagazineUI : NetworkBehaviour
{
    public Slider magazineSlider;
    public GameObject basicReference;
    private BasicGun basicGun;

    void Update()
    {
        if (!IsOwner) return;

        if (basicGun != null)
        {
            magazineSlider.maxValue = basicGun.magazineSize;
            magazineSlider.value = basicGun.currentMagazine;
        }
        else
        {
            magazineSlider.value = 0;
        }
        UpdateMagazineUI();
    }

    public void SetBasicGun(BasicGun gun)
    {
        basicGun = gun;
    }

    void UpdateMagazineUI()
    {
        if (!IsOwner) return;

        BasicGun currentGun = basicReference.GetComponentInChildren<BasicGun>();
        SetBasicGun(currentGun);
    }
}
