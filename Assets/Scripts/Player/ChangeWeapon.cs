using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWeapon : MonoBehaviour
{
    public GameObject BaseWeaponReference;
    public GameObject AdditionalWeaponReference;
    public GameObject UIBaseWeaponReference;
    public GameObject UIAdditionalWeaponReference;
    private bool isUsingBaseWeapon = true;
    private AdditionalGun additionalGun;
    private BasicGun basicGun;

    void Start()
    {
        additionalGun = AdditionalWeaponReference.GetComponentInChildren<AdditionalGun>();
        basicGun = BaseWeaponReference.GetComponentInChildren<BasicGun>();

        BaseWeaponReference.SetActive(true);
        AdditionalWeaponReference.SetActive(false);
        UIBaseWeaponReference.SetActive(true);
        UIAdditionalWeaponReference.SetActive(false);
    }

    void Update()
    {
        additionalGun = AdditionalWeaponReference.GetComponentInChildren<AdditionalGun>();
        basicGun = BaseWeaponReference.GetComponentInChildren<BasicGun>();

        if (Input.GetAxis("Mouse ScrollWheel") != 0f && additionalGun != null && !IsAnyWeaponReloading())
        {
            SwitchWeapon();
        }
    }

    bool IsAnyWeaponReloading()
    {
        return (additionalGun != null && additionalGun.isReloading) || (basicGun != null && basicGun.isReloading);
    }

    void SwitchWeapon()
    {
        isUsingBaseWeapon = !isUsingBaseWeapon;

        BaseWeaponReference.SetActive(isUsingBaseWeapon);
        AdditionalWeaponReference.SetActive(!isUsingBaseWeapon);

        UIBaseWeaponReference.SetActive(isUsingBaseWeapon);
        UIAdditionalWeaponReference.SetActive(!isUsingBaseWeapon);
    }
}