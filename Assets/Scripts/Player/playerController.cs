using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerController : NetworkBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    [Header("Movement")]
    public float movSpeed = 5f;

    [Header("Dash")]
    public float dashSpeed = 15f;
    public float dashDuration = 0.2f;
    public float dashCooldown = 1f;
    public float invulnerabilityDuration = 0.5f;

    public Collider2D damageCollider;

    private Vector2 movement;
    private bool isDashing = false;
    private bool canDash = true;
    private float dashTime;
    private float cooldownTime;
    private bool isInvulnerable = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (!IsOwner) return;
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        movement = movement.normalized;

        UpdateAnimations();

        if (Input.GetKeyDown(KeyCode.Space) && canDash && !isDashing)
        {
            StartDash();
        }

        if (isDashing)
        {
            dashTime -= Time.deltaTime;
            if (dashTime <= 0)
            {
                EndDash();
            }
        }

        if (!canDash)
        {
            cooldownTime -= Time.deltaTime;
            if (cooldownTime <= 0)
            {
                canDash = true;
            }
        }
    }

    void FixedUpdate()
    {
        if (isDashing)
        {
            rb.MovePosition(rb.position + movement * dashSpeed * Time.fixedDeltaTime);
        }
        else
        {
            rb.MovePosition(rb.position + movement * movSpeed * Time.fixedDeltaTime);
        }
    }

    void StartDash()
    {
        isDashing = true;
        canDash = false;
        dashTime = dashDuration;
        cooldownTime = dashCooldown;
        StartCoroutine(InvulnerabilityCoroutine(dashDuration + invulnerabilityDuration));
    }

    void EndDash()
    {
        isDashing = false;
    }

    private IEnumerator InvulnerabilityCoroutine(float duration)
    {
        SetInvulnerability(true);
        yield return new WaitForSeconds(duration);
        SetInvulnerability(false);
    }

    private void SetInvulnerability(bool state)
    {
        isInvulnerable = state;
        damageCollider.enabled = !state;
    }

    private void UpdateAnimations()
    {
        if (movement.x != 0 || movement.y != 0)
        {
            animator.SetFloat("Blend", 1);
            animator.SetFloat("MoveX", movement.x);
            animator.SetFloat("MoveY", movement.y);

            if (movement.x < 0)
            {
                spriteRenderer.flipX = true;
            }
            else if (movement.x > 0)
            {
                spriteRenderer.flipX = false;
            }
        }
        else
        {
            animator.SetFloat("Blend", 0);
        }
    }
}