using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Unity.Netcode;

public class WeaponUI : NetworkBehaviour
{
    public TextMeshProUGUI ammoText;
    public GameObject additionalReference;
    public Image weaponImage;
    private AdditionalGun additionalGun;

    void Update()
    {
        if (!IsOwner) return;

        if (additionalGun != null)
        {
            ammoText.text = additionalGun.currentAmmo + " / " + additionalGun.maxAmmo;
            weaponImage.sprite = additionalGun.spriteArmaUI;
        }
        else
        {
            ammoText.text = "No Weapon";
            weaponImage.sprite = null;
        }
        UpdateAmmoUI();
    }

    public void SetAdditionalGun(AdditionalGun gun)
    {
        additionalGun = gun;
    }

    void UpdateAmmoUI()
    {
        if (!IsOwner) return;

        AdditionalGun currentGun = additionalReference.GetComponentInChildren<AdditionalGun>();
        SetAdditionalGun(currentGun);
    }
}
