using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class CrystalPool : MonoBehaviour
{
    [SerializeField] private GameObject crystalPrefab;
    [SerializeField] private int poolSize = 50;
    [SerializeField] private List<GameObject> crystalList;

    private static CrystalPool instance;
    public static CrystalPool Instance { get { return instance; } }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        crystalList = new List<GameObject>();
        AddCrystalToPool(poolSize);
    }

    private void AddCrystalToPool(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            GameObject crystal = Instantiate(crystalPrefab);
            crystal.SetActive(false);
            crystal.transform.SetParent(transform);
            crystalList.Add(crystal);
        }
    }

    public GameObject RequestCrystal(Vector3 position)
    {
        GameObject crystal = GetInactiveCrystal();
        crystal.transform.position = position;
        crystal.SetActive(true);

        if (!crystal.GetComponent<NetworkObject>().IsSpawned)
        {
            crystal.GetComponent<NetworkObject>().Spawn(true);
        }

        return crystal;
    }

    private GameObject GetInactiveCrystal()
    {
        foreach (GameObject crystal in crystalList)
        {
            if (!crystal.activeSelf)
            {
                return crystal;
            }
        }

        GameObject newCrystal = Instantiate(crystalPrefab);
        newCrystal.SetActive(false);
        newCrystal.transform.SetParent(transform);
        crystalList.Add(newCrystal);
        return newCrystal;
    }
}
