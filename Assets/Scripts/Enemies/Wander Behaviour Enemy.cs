using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WanderBehaviourEnemy : MonoBehaviour
{
    public float wanderRadius = 10f;
    public float minWaitTime = 2f;
    public float maxWaitTime = 5f;

    private NavMeshAgent navMeshAgent;
    private bool isWandering = false;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.updateRotation = false; 
        navMeshAgent.updateUpAxis = false; 
        StartCoroutine(Wander());
        transform.rotation = Quaternion.Euler(0, 0, 0);
    }


    private IEnumerator Wander()
    {
        while (true)
        {
            if (!isWandering)
            {
                Vector3 newDestination = GetRandomPoint(transform.position, wanderRadius);
                navMeshAgent.SetDestination(newDestination);
                isWandering = true;
            }

            if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                isWandering = false;
                float waitTime = Random.Range(minWaitTime, maxWaitTime);
                yield return new WaitForSeconds(waitTime);
            }

            yield return null;
        }
    }

    private Vector3 GetRandomPoint(Vector3 center, float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += center;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }
}