using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySummon : MonoBehaviour
{
    [Header("Summon Settings")]
    public GameObject enemyPrefab;
    public float summonRadius = 5f;
    public int enemiesPerSummon = 3;

    [Header("Cooldown Settings")]
    public float summonCooldown = 10f;
    public float summonWaitTime = 2f;

    [Header("Movement Settings")]
    public bool stopWhenSummoning = true;
    private bool isSummoning = false;
    private NavMeshAgent navMeshAgent;
    private Animator animator;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        StartCoroutine(SummonEnemies());
    }

    private IEnumerator SummonEnemies()
    {
        while (true)
        {
            if (stopWhenSummoning && navMeshAgent != null)
            {
                navMeshAgent.isStopped = true;
            }

            isSummoning = true;

            // Activar animación de carga de invocación
            animator.SetBool("IsSummoning", true);
            yield return new WaitForSeconds(summonWaitTime);
            animator.SetBool("IsSummoning", false);

            for (int i = 0; i < enemiesPerSummon; i++)
            {
                Vector3 summonPosition = GetValidSummonPosition();
                if (summonPosition != Vector3.zero)
                {
                    GameObject summonedEnemy = Instantiate(enemyPrefab, summonPosition, Quaternion.identity);
                    summonedEnemy.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            }

            yield return new WaitForSeconds(summonWaitTime);

            isSummoning = false;

            if (stopWhenSummoning && navMeshAgent != null)
            {
                navMeshAgent.isStopped = false;
            }

            yield return new WaitForSeconds(summonCooldown);
        }
    }

    private Vector3 GetValidSummonPosition()
    {
        for (int i = 0; i < 10; i++)
        {
            Vector3 randomDirection = Random.insideUnitSphere * summonRadius;
            randomDirection += transform.position;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomDirection, out hit, summonRadius, NavMesh.AllAreas))
            {
                return hit.position;
            }
        }
        return Vector3.zero;
    }
}