using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFirePointsRotation : MonoBehaviour
{
    public float rotationSpeed = 5f; 

    private GameObject targetPlayer;

    private void Start()
    {
        StartCoroutine(SearchForPlayer());
    }

    private void Update()
    {
        if (targetPlayer != null)
        {
            Vector2 direction = (targetPlayer.transform.position - transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;

            Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle));
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
        }
    }

    private IEnumerator SearchForPlayer()
    {
        while (true)
        {
            FindClosestPlayer();
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void FindClosestPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        float closestDistance = Mathf.Infinity;
        GameObject closestPlayer = null;

        foreach (GameObject player in players)
        {
            float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
            if (distanceToPlayer < closestDistance)
            {
                closestDistance = distanceToPlayer;
                closestPlayer = player;
            }
        }

        targetPlayer = closestPlayer;
    }
}
