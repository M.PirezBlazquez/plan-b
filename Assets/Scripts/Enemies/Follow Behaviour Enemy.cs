using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FollowBehaviourEnemy : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private GameObject targetPlayer;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    public float searchInterval = 0.5f;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        navMeshAgent.updateRotation = false;
        navMeshAgent.updateUpAxis = false;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        StartCoroutine(SearchForPlayer());
    }

    private void FixedUpdate()
    {
        if (targetPlayer != null)
        {
            navMeshAgent.SetDestination(targetPlayer.transform.position);
            UpdateAnimations();
        }
    }

    private IEnumerator SearchForPlayer()
    {
        while (true)
        {
            FindClosestPlayer();
            yield return new WaitForSeconds(searchInterval);
        }
    }

    private void FindClosestPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        float closestDistance = Mathf.Infinity;
        GameObject closestPlayer = null;

        foreach (GameObject player in players)
        {
            float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
            if (distanceToPlayer < closestDistance)
            {
                closestDistance = distanceToPlayer;
                closestPlayer = player;
            }
        }

        targetPlayer = closestPlayer;
    }

    private void UpdateAnimations()
    {
        Vector3 velocity = navMeshAgent.velocity;
        animator.SetFloat("Blend", velocity.magnitude);
        animator.SetFloat("MoveX", velocity.x);
        animator.SetFloat("MoveY", velocity.y);

        if (velocity.x < 0)
        {
            spriteRenderer.flipX = false;
        }
        else if (velocity.x > 0)
        {
            spriteRenderer.flipX = true;
        }
    }
}