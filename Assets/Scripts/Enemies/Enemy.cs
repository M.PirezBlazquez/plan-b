using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class Enemy : NetworkBehaviour
{
    public int maxHealth = 10;
    [SerializeField] private NetworkVariable<int> currentHealth = new NetworkVariable<int>();

    private int contactDamage = 1;
    private bool isDead = false;

    [Header("Drop Crystals")]
    [SerializeField] public GameObject crystalPrefab;

    public int crystalCount = 5;

    [Header("Death Effect")]
    public bool deathEffect = false;

    [Header("Particles")]
    public ParticleSystem damageParticlePrefab;
    public ParticleSystem deathParticlePrefab;

    private ParticleSystem damageParticleInstance;

    public override void OnNetworkSpawn()
    {
        if (IsServer)
        {
            currentHealth.Value = maxHealth;
        }

        if (damageParticlePrefab != null)
        {
            damageParticleInstance = Instantiate(damageParticlePrefab, transform.position, Quaternion.identity, transform);
            damageParticleInstance.Stop();
        }

        currentHealth.OnValueChanged += OnHealthChanged;
    }

    private void OnDestroy()
    {
        currentHealth.OnValueChanged -= OnHealthChanged;
    }

    private void OnHealthChanged(int previousValue, int newValue)
    {
        if (newValue <= 0 && !isDead)
        {
            isDead = true;
            Die();
        }
    }

    public void TakeDamage(int damage)
    {
        if (!IsServer)
            return;

        currentHealth.Value -= damage;

        PlayDamageParticlesClientRpc();

        if (currentHealth.Value <= 0 && !isDead)
        {
            isDead = true;
            Die();
        }
    }

    private void Die()
    {
        if (IsServer)
        {
            int crystalCount = this.crystalCount;
            DropCrystalsClientRpc(crystalCount, transform.position);
        }

        PlayDeathParticlesClientRpc();

        if (deathEffect)
        {
            EnemyDeathEffect deathEffectScript = GetComponent<EnemyDeathEffect>();
            if (deathEffectScript != null)
            {
                deathEffectScript.TriggerDeathEffect();
            }
        }

        Destroy(gameObject);
    }

    [ClientRpc]
    private void DropCrystalsClientRpc(int crystalCount, Vector3 position)
    {
        for (int i = 0; i < crystalCount; i++)
        {
            GameObject crystal = CrystalPool.Instance.RequestCrystal(position);
            NetworkObject networkObject = crystal.GetComponent<NetworkObject>();
            if (networkObject != null && !networkObject.IsSpawned)
            {
                networkObject.Spawn(true);
            }
        }
    }

    [ClientRpc]
    private void PlayDamageParticlesClientRpc()
    {
        if (damageParticleInstance != null)
        {
            damageParticleInstance.Play();
        }
    }

    [ClientRpc]
    private void PlayDeathParticlesClientRpc()
    {
        if (deathParticlePrefab != null)
        {
            ParticleSystem deathParticleInstance = Instantiate(deathParticlePrefab, transform.position, Quaternion.identity);
            deathParticleInstance.Play();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Bullet"))
        {
            Bullet bullet = collision.GetComponent<Bullet>();
            if (bullet != null)
            {
                TakeDamageServerRpc(bullet.damage);
                Destroy(collision.gameObject);
            }
        }
        else if (collision.CompareTag("Player"))
        {
            PlayerHP playerHP = collision.GetComponent<PlayerHP>();
            if (playerHP != null)
            {
                playerHP.TakeDamage(contactDamage);
            }
        }
    }

    [ServerRpc]
    private void TakeDamageServerRpc(int damage)
    {
        if (!IsServer)
            return;

        TakeDamage(damage);
    }
}
