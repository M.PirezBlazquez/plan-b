using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShoot : MonoBehaviour
{
    [Header("Shooting")]
    public Transform[] firePoints;
    public GameObject bulletPrefab;
    public float bulletSpeed = 10f;
    public float shootingRange = 20f;

    public float minCooldownTime = 2f;
    public float maxCooldownTime = 4f;
    private float nextFireTime = 0f;

    [Header("Charge Time")]
    public bool hasChargeTime = false;
    public float chargeTime = 1f;

    [Header("Burst")]
    public bool isBurst = false;
    public int burstCount = 3;
    public float burstInterval = 0.1f;

    [Header("Stop When Shooting")]
    public bool stopWhenShooting = false;
    public float stopAfterShootingTime = 1f;
    private bool isShooting = false;
    private NavMeshAgent navMeshAgent;
    private Transform player;
    private Animator animator;

    [Header("Particles")]
    public ParticleSystem chargeParticle;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        animator = GetComponent<Animator>();
        SetNextFireTime();

        if (chargeParticle != null)
        {
            chargeParticle.Stop();
        }
    }

    private void Update()
    {
        if (Time.time >= nextFireTime && IsPlayerInRange())
        {
            if (hasChargeTime)
            {
                StartCoroutine(ChargeAndShoot());
            }
            else if (isBurst)
            {
                StartCoroutine(BurstShoot());
            }
            else
            {
                Shoot();
            }

            SetNextFireTime();
        }

        if (stopWhenShooting)
        {
            navMeshAgent.isStopped = isShooting;
        }
    }

    private bool IsPlayerInRange()
    {
        if (player == null) return false;
        return Vector3.Distance(transform.position, player.position) <= shootingRange;
    }

    private void SetNextFireTime()
    {
        nextFireTime = Time.time + Random.Range(minCooldownTime, maxCooldownTime);
    }

    private IEnumerator ChargeAndShoot()
    {
        isShooting = true;
        if (stopWhenShooting && navMeshAgent != null)
        {
            navMeshAgent.isStopped = true;
        }

        if (chargeParticle != null)
        {
            chargeParticle.Play();
        }

        animator.SetBool("IsCharging", true);
        yield return new WaitForSeconds(chargeTime);
        animator.SetBool("IsCharging", false);

        if (chargeParticle != null)
        {
            chargeParticle.Stop();
        }

        if (isBurst)
        {
            yield return BurstShoot();
        }
        else
        {
            Shoot();
        }

        yield return new WaitForSeconds(stopAfterShootingTime);

        isShooting = false;
        if (stopWhenShooting && navMeshAgent != null)
        {
            navMeshAgent.isStopped = false;
        }
    }

    private IEnumerator BurstShoot()
    {
        isShooting = true;
        for (int i = 0; i < burstCount; i++)
        {
            Shoot();
            yield return new WaitForSeconds(burstInterval);
        }

        yield return new WaitForSeconds(stopAfterShootingTime);

        isShooting = false;
    }

    private void Shoot()
    {
        animator.SetBool("IsShooting", true);
        foreach (Transform firePoint in firePoints)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            EnemyBullet enemyBullet = bullet.GetComponent<EnemyBullet>();
            if (enemyBullet != null)
            {
                enemyBullet.SetDirection(firePoint.up, bulletSpeed);
            }
            else
            {
                Debug.LogError("EnemyBullet componente no encontrado en el prefab de la bala");
            }
        }
        StartCoroutine(ResetShootingAnimation());
    }

    private IEnumerator ResetShootingAnimation()
    {
        yield return null;
        animator.SetBool("IsShooting", false);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootingRange);
    }
}