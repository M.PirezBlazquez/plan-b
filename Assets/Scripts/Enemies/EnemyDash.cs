using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyDash : MonoBehaviour
{
    public float detectionRange = 10f; 
    public float chargeTime = 1f;
    public float dashSpeed = 20f; 
    public float stopTime = 0.5f;
    public float cooldownTime = 3f; 

    private NavMeshAgent navMeshAgent;
    private Transform player;
    private bool isDashing = false;
    private bool isOnCooldown = false;
    private Vector3 dashDirection;
    private Animator animator;

    [Header("Particles")]
    public ParticleSystem dashParticle;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        if (dashParticle != null)
        {
            dashParticle.Stop();
        }
    }

    private void Update()
    {
        if (isDashing || isOnCooldown) return;

        player = DetectPlayer();
        if (player != null)
        {
            StartCoroutine(DashRoutine());
        }
        else
        {
            navMeshAgent.isStopped = false;
        }
    }

    private Transform DetectPlayer()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, detectionRange);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.CompareTag("Player"))
            {
                return hitCollider.transform;
            }
        }
        return null;
    }

    private IEnumerator DashRoutine()
    {
        isDashing = true;
        navMeshAgent.isStopped = true;

        animator.SetBool("IsCharging", true);
        yield return new WaitForSeconds(chargeTime);
        animator.SetBool("IsCharging", false);

        if (player != null)
        {
            dashDirection = (player.position - transform.position).normalized;
        }

        float originalSpeed = navMeshAgent.speed;
        navMeshAgent.speed = dashSpeed;
        navMeshAgent.isStopped = false;
        navMeshAgent.velocity = dashDirection * dashSpeed;

        animator.SetBool("IsDashing", true);
        if (dashParticle != null)
        {
            dashParticle.Play();
        }

        yield return new WaitForSeconds(stopTime);

        if (dashParticle != null)
        {
            dashParticle.Stop();
        }
        animator.SetBool("IsDashing", false);

        navMeshAgent.isStopped = true;
        navMeshAgent.speed = originalSpeed;
        navMeshAgent.velocity = Vector3.zero;

        isDashing = false;

        StartCoroutine(CooldownRoutine());
    }

    private IEnumerator CooldownRoutine()
    {
        isOnCooldown = true;
        yield return new WaitForSeconds(cooldownTime);
        isOnCooldown = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, detectionRange);
    }
}