using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class EnemyBullet : NetworkBehaviour
{
    public float speed = 10f;
    public float lifetime = 5f;
    public int damage = 1;

    private Rigidbody2D rb;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        rb = GetComponent<Rigidbody2D>();
        if (rb == null)
        {
            Debug.LogError("Rigidbody2D no encontrado en la bala");
        }
        Destroy(gameObject, lifetime);
    }

    public void SetDirection(Vector2 direction, float newSpeed)
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                Debug.LogError("Rigidbody2D no encontrado en la bala en SetDirection");
                return;
            }
        }
        speed = newSpeed;
        rb.velocity = direction * speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (IsServer)
        {
            if (collision.CompareTag("Player"))
            {
                PlayerHP playerHP = collision.GetComponent<PlayerHP>();
                if (playerHP != null)
                {
                    playerHP.TakeDamage(damage);
                }
                DestroyBulletClientRpc();
            }
        }
    }

    [ClientRpc]
    private void DestroyBulletClientRpc()
    {
        Destroy(gameObject);
    }
}
