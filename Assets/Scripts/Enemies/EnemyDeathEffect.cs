using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class EnemyDeathEffect : NetworkBehaviour
{
    [Header("Bullet Effect")]
    public bool shootBulletsOnDeath = false;
    public GameObject bulletPrefab;
    public List<Transform> firePoints;
    public float bulletSpeed = 10f;

    [Header("Summon Effect")]
    public bool summonEnemiesOnDeath = false;
    public List<GameObject> enemyPrefabs;
    public List<Transform> summonPoints;

    private bool effectTriggered = false;

    public void TriggerDeathEffect()
    {
        if (!effectTriggered)
        {
            effectTriggered = true;

            if (shootBulletsOnDeath)
            {
                ShootBulletsServerRpc();
            }

            if (summonEnemiesOnDeath)
            {
                SummonEnemiesServerRpc();
            }
        }
    }

    [ServerRpc]
    private void ShootBulletsServerRpc()
    {
        foreach (Transform firePoint in firePoints)
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            bullet.GetComponent<NetworkObject>().Spawn();
            EnemyBullet enemyBullet = bullet.GetComponent<EnemyBullet>();
            if (enemyBullet != null)
            {
                Vector2 direction = firePoint.up;
                enemyBullet.SetDirection(direction, bulletSpeed);
            }
        }
    }

    [ServerRpc]
    private void SummonEnemiesServerRpc()
    {
        foreach (Transform summonPoint in summonPoints)
        {
            GameObject randomEnemyPrefab = enemyPrefabs[Random.Range(0, enemyPrefabs.Count)];
            GameObject enemy = Instantiate(randomEnemyPrefab, summonPoint.position, Quaternion.identity);
            enemy.GetComponent<NetworkObject>().Spawn();
        }
    }
}