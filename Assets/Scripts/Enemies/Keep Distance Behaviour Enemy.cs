using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class KeepDistanceBehaviourEnemy : MonoBehaviour
{
    public float safeDistance = 5f; 
    public float searchInterval = 0.5f; 

    public float Kp = 1.0f; // proporcional
    public float Ki = 0.1f; // integral
    public float Kd = 0.1f; // derivativo

    public float maxSpeed = 5f; // Velocidad m�xima

    private NavMeshAgent navMeshAgent;
    private GameObject targetPlayer;

    private float integral;
    private float previousError;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.updateRotation = false; 
        navMeshAgent.updateUpAxis = false; 

        StartCoroutine(SearchForPlayer());
    }

    private void FixedUpdate()
    {
        if (targetPlayer != null)
        {
            float distanceToPlayer = Vector2.Distance(transform.position, targetPlayer.transform.position);
            float error = distanceToPlayer - safeDistance;

            integral += error * Time.deltaTime;
            float derivative = (error - previousError) / Time.deltaTime;
            float output = Kp * error + Ki * integral + Kd * derivative;

            Vector2 direction = (targetPlayer.transform.position - transform.position).normalized;
            Vector2 movement = direction * output;

            if (movement.magnitude > maxSpeed)
            {
                movement = movement.normalized * maxSpeed;
            }

            navMeshAgent.velocity = new Vector2(movement.x, movement.y);

            previousError = error;
        }
    }

    private IEnumerator SearchForPlayer()
    {
        while (true)
        {
            FindClosestPlayer();
            yield return new WaitForSeconds(searchInterval);
        }
    }

    private void FindClosestPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        float closestDistance = Mathf.Infinity;
        GameObject closestPlayer = null;

        foreach (GameObject player in players)
        {
            float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
            if (distanceToPlayer < closestDistance)
            {
                closestDistance = distanceToPlayer;
                closestPlayer = player;
            }
        }

        targetPlayer = closestPlayer;
    }
}