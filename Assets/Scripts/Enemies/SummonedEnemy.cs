using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SummonedEnemy : MonoBehaviour
{
    public float summoningTime = 2f;
    public MonoBehaviour[] scriptsToActivate;
    private NavMeshAgent navMeshAgent;
    private Animator animator;

    [Header("Particles")]
    public ParticleSystem summoningParticle;

    private void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        navMeshAgent.updateRotation = false;
        navMeshAgent.updateUpAxis = false;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        foreach (var script in scriptsToActivate)
        {
            script.enabled = false;
        }
        StartCoroutine(ActivateScriptsAfterSummoning());
    }

    private IEnumerator ActivateScriptsAfterSummoning()
    {
        animator.SetBool("IsSpawning", true);

        if (summoningParticle != null)
        {
            summoningParticle.Play();
        }

        yield return new WaitForSeconds(summoningTime);

        animator.SetBool("IsSpawning", false);

        if (summoningParticle != null)
        {
            summoningParticle.Stop();
        }

        foreach (var script in scriptsToActivate)
        {
            script.enabled = true;
        }

        this.enabled = false;
    }
}