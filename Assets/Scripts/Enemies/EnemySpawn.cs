using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Unity.Netcode;

public class EnemySpawn : NetworkBehaviour
{
    [Header("Spawn Settings")]
    public GameObject[] enemyPrefabs;
    public float spawnRadius = 30f;
    public float minDistanceFromPlayers = 20f;
    public float spawnInterval = 10f;
    public int numberOfEnemies = 1;

    private List<Transform> players;

    private void Start()
    {
        if (IsServer)
        {
            players = new List<Transform>();
            StartCoroutine(UpdatePlayerList());
            StartCoroutine(SpawnEnemies());
        }
    }

    private IEnumerator UpdatePlayerList()
    {
        while (true)
        {
            players.Clear();
            foreach (var client in NetworkManager.Singleton.ConnectedClientsList)
            {
                if (client.PlayerObject != null)
                {
                    players.Add(client.PlayerObject.transform);
                }
            }
            yield return new WaitForSeconds(5f);
        }
    }

    private IEnumerator SpawnEnemies()
    {
        while (true)
        {
            for (int i = 0; i < numberOfEnemies; i++)
            {
                Vector3 spawnPosition = GetValidSpawnPosition();
                if (spawnPosition != Vector3.zero)
                {
                    SpawnEnemyServerRpc(spawnPosition);
                }
            }
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    private Vector3 GetValidSpawnPosition()
    {
        for (int i = 0; i < 10; i++)
        {
            Vector3 randomDirection = Random.insideUnitSphere * spawnRadius;
            randomDirection += transform.position;

            if (NavMesh.SamplePosition(randomDirection, out NavMeshHit hit, spawnRadius, NavMesh.AllAreas))
            {
                if (IsValidSpawnPosition(hit.position))
                {
                    return hit.position;
                }
            }
        }
        return Vector3.zero;
    }

    private bool IsValidSpawnPosition(Vector3 position)
    {
        foreach (var player in players)
        {
            if (Vector3.Distance(position, player.position) < minDistanceFromPlayers || IsInFieldOfView(player, position))
            {
                return false;
            }
        }
        return true;
    }

    private bool IsInFieldOfView(Transform player, Vector3 position)
    {
        Vector3 directionToTarget = (position - player.position).normalized;
        float angle = Vector3.Angle(player.forward, directionToTarget);
        return angle < 90f;
    }

    [ServerRpc(RequireOwnership = false)]
    private void SpawnEnemyServerRpc(Vector3 spawnPosition)
    {
        int randomIndex = Random.Range(0, enemyPrefabs.Length);
        GameObject enemy = Instantiate(enemyPrefabs[randomIndex], spawnPosition, Quaternion.identity);
        enemy.GetComponent<NetworkObject>().Spawn();
    }
}