using UnityEngine;

public class TestActivateScript : MonoBehaviour
{
    // Asigna el script que quieres activar en el inspector
    public MonoBehaviour scriptToActivate;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (scriptToActivate != null)
            {
                scriptToActivate.enabled = !scriptToActivate.enabled;
                Debug.Log(scriptToActivate.GetType().Name + " is now " + (scriptToActivate.enabled ? "enabled" : "disabled"));
            }
            else
            {
                Debug.LogWarning("No script assigned to be activated/deactivated.");
            }
        }
    }
}