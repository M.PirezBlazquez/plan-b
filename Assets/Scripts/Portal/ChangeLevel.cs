using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Netcode;

public class ChangeLevel : NetworkBehaviour
{
    public string[] scenes;
    public float waitTime = 2.0f;
    private string nextScene;

    private void Start()
    {
        if (scenes.Length > 0)
        {
            nextScene = scenes[Random.Range(0, scenes.Length)];
        }
        else
        {
            Debug.LogWarning("No hay escenas configuradas en el array.");
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void ChangeLevelServerRpc()
    {
        if (!string.IsNullOrEmpty(nextScene))
        {
            StartCoroutine(ChangeToNextLevel());
        }
        else
        {
            Debug.LogError("No se ha seleccionado una escena v�lida.");
        }
    }

    private IEnumerator ChangeToNextLevel()
    {
        yield return new WaitForSeconds(waitTime);

        foreach (var client in NetworkManager.Singleton.ConnectedClientsList)
        {
            var playerObject = client.PlayerObject;
            DontDestroyOnLoad(playerObject);
        }

        NetworkManager.Singleton.SceneManager.LoadScene(nextScene, LoadSceneMode.Single);

        yield return new WaitForSeconds(0.1f);

        foreach (var client in NetworkManager.Singleton.ConnectedClientsList)
        {
            var playerObject = client.PlayerObject;
            SceneManager.MoveGameObjectToScene(playerObject.gameObject, SceneManager.GetActiveScene());
        }
    }

    [ClientRpc]
    public void TriggerChangeLevelClientRpc()
    {
        if (IsOwner)
        {
            StartCoroutine(ChangeToNextLevel());
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && IsOwner)
        {
            ChangeLevelServerRpc();
        }
    }
}
