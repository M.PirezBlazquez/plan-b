using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PortalCollectCrystals : NetworkBehaviour
{
    public int maxCrystals = 50;
    public float collectionInterval = 0.1f;
    public Collider2D collectionArea;
    public Collider2D activationArea;
    public ChangeLevel changeLevelScript;

    private NetworkVariable<int> currentCrystals = new NetworkVariable<int>();
    private bool isCollecting = false;
    private CrystalInventory playerInventory;
    private bool portalActivated = false;

    private void Start()
    {
        collectionArea.enabled = true;
        activationArea.enabled = false;
        changeLevelScript.enabled = false;

        currentCrystals.OnValueChanged += OnCrystalsChanged;
    }

    private void OnDestroy()
    {
        currentCrystals.OnValueChanged -= OnCrystalsChanged;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerInventory = collision.GetComponent<CrystalInventory>();
            if (playerInventory != null && !isCollecting)
            {
                StartCoroutine(CollectCrystals());
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            StopCoroutine(CollectCrystals());
            isCollecting = false;
        }
    }

    private IEnumerator CollectCrystals()
    {
        isCollecting = true;
        while (currentCrystals.Value < maxCrystals && playerInventory.GetCurrentCrystals() > 0)
        {
            playerInventory.UseCrystal();
            UpdateCrystalsServerRpc(1);

            yield return new WaitForSeconds(collectionInterval);

            if (currentCrystals.Value >= maxCrystals)
            {
                break;
            }
        }

        isCollecting = false;
    }

    [ServerRpc(RequireOwnership = false)]
    private void UpdateCrystalsServerRpc(int amount)
    {
        currentCrystals.Value += amount;
    }

    private void ActivatePortal()
    {
        collectionArea.enabled = false;
        activationArea.enabled = true;
        changeLevelScript.enabled = true;
        portalActivated = true;
    }

    [ClientRpc]
    private void ActivatePortalClientRpc()
    {
        ActivatePortal();
    }

    private void OnCrystalsChanged(int previousValue, int newValue)
    {
        if (newValue >= maxCrystals)
        {
            ActivatePortalClientRpc();
        }
    }

    private void Update()
    {
        if (portalActivated && Input.GetKeyDown(KeyCode.E) && IsOwner)
        {
            changeLevelScript.ChangeLevelServerRpc();
        }
    }
}
