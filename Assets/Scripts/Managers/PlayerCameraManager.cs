using UnityEngine;
using Unity.Netcode;
using Cinemachine;

public class PlayerCameraManager : NetworkBehaviour
{
    public CinemachineVirtualCamera virtualCamera;

    void Start()
    {
        if (!IsOwner && virtualCamera != null)
        {
            virtualCamera.gameObject.SetActive(false);
        }
        else if (IsOwner && virtualCamera != null)
        {
            virtualCamera.gameObject.SetActive(true);
        }
    }
}
