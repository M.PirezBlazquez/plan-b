using UnityEngine;
using Unity.Netcode;

public class PlayerUIManager : NetworkBehaviour
{
    public GameObject playerUI;

    void Start()
    {
        if (!IsOwner && playerUI != null)
        {
            Destroy(playerUI);
        }
    }
}