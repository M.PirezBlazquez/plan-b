using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class NetworkManagerUI : MonoBehaviour
{
    [SerializeField] private Button serverB;
    [SerializeField] private Button hostB;
    [SerializeField] private Button clientB;

    private void Awake()
    {
        serverB.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartServer();
        });

        hostB.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartHost();
        });

        clientB.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartClient();
        });
    }
}
