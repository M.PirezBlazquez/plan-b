using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class BasicGun : NetworkBehaviour
{
    public Transform firePoint;
    public GameObject bulletPrefab;

    [Header("AMMO")]
    public int magazineSize = 10;
    public int currentMagazine;

    [Header("Fire Rate")]
    public float timeBetweenShots = 0.5f;
    public float nextTimeToFire = 0f;

    [Header("Reload")]
    public float reloadTime = 2f;
    public bool isReloading = false;

    [Header("Animation")]
    public Animator animator;

    void Start()
    {
        currentMagazine = magazineSize;
        UpdateIdleAnimation();
    }

    void Update()
    {
        if (!IsOwner) return;

        if (isReloading)
            return;

        if (currentMagazine <= 0)
        {
            StartReloadServerRpc();
            return;
        }

        if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire)
        {
            if (currentMagazine > 0)
            {
                ShootServerRpc(firePoint.position, firePoint.rotation.eulerAngles);
                nextTimeToFire = Time.time + timeBetweenShots;
            }
        }

        if (Input.GetKeyDown(KeyCode.R) && currentMagazine < magazineSize)
        {
            StartReloadServerRpc();
        }
    }

    [ServerRpc]
    void ShootServerRpc(Vector3 position, Vector3 rotation, ServerRpcParams rpcParams = default)
    {
        Quaternion adjustedRotation = Quaternion.Euler(rotation) * Quaternion.Euler(0, 0, -90);
        GameObject bullet = Instantiate(bulletPrefab, position, adjustedRotation);
        bullet.GetComponent<NetworkObject>().Spawn();
        currentMagazine--;

        UpdateMagazineClientRpc(currentMagazine);
        ShootClientRpc();
    }

    [ClientRpc]
    void ShootClientRpc(ClientRpcParams rpcParams = default)
    {
        animator.SetBool("IsShooting", true);
        StartCoroutine(ResetShootingAnimation());
        UpdateIdleAnimation();
    }

    [ClientRpc]
    void UpdateMagazineClientRpc(int newMagazine)
    {
        currentMagazine = newMagazine;
        UpdateIdleAnimation();
    }

    [ServerRpc]
    void StartReloadServerRpc(ServerRpcParams rpcParams = default)
    {
        if (!isReloading)
        {
            StartCoroutine(Reload());
        }
    }

    IEnumerator Reload()
    {
        isReloading = true;
        UpdateReloadingClientRpc(true);

        Debug.Log("Recargando...");

        animator.SetBool("IsReloading", true);

        yield return new WaitForSeconds(reloadTime);

        currentMagazine = magazineSize;
        isReloading = false;
        UpdateReloadingClientRpc(false);

        animator.SetBool("IsReloading", false);
        UpdateMagazineClientRpc(currentMagazine);
        UpdateIdleAnimation();
    }

    [ClientRpc]
    void UpdateReloadingClientRpc(bool reloading)
    {
        isReloading = reloading;
    }

    IEnumerator ResetShootingAnimation()
    {
        yield return null;
        animator.SetBool("IsShooting", false);
    }

    void UpdateIdleAnimation()
    {
        if (currentMagazine > 0)
        {
            animator.SetBool("HasAmmo", true);
        }
        else
        {
            animator.SetBool("HasAmmo", false);
        }
    }
}
