using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class Bullet : NetworkBehaviour
{
    public float speed = 20f;
    public int damage = 10;
    public float lifetime = 5f;

    [Header("Explosion")]
    public bool Explosive = false;
    public float explosionArea = 5f;

    [Header("Penetration")]
    public bool penetration = false;

    private Rigidbody2D rb;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsServer)
        {
            rb = GetComponent<Rigidbody2D>();
            rb.velocity = transform.up * speed;
            StartCoroutine(DestroyAfterLifetime());
        }
    }

    public void SetDirection(Vector2 direction, float newSpeed)
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody2D>();
            if (rb == null)
            {
                Debug.LogError("Rigidbody2D no encontrado en la bala en SetDirection");
                return;
            }
        }
        speed = newSpeed;
        rb.velocity = direction * speed;
    }

    private IEnumerator DestroyAfterLifetime()
    {
        yield return new WaitForSeconds(lifetime);
        DestroyBulletServerRpc();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!IsServer) return;

        if (collision.CompareTag("Enemy"))
        {
            Enemy enemy = collision.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }

            if (Explosive)
            {
                Explode();
            }

            if (!penetration)
            {
                DestroyBulletServerRpc();
            }
        }

        if (collision.CompareTag("Wall"))
        {
            DestroyBulletServerRpc();
        }
    }

    private void Explode()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, explosionArea);
        foreach (Collider2D nearbyObject in colliders)
        {
            Enemy enemy = nearbyObject.GetComponent<Enemy>();
            if (enemy != null)
            {
                enemy.TakeDamage(damage);
            }
        }
    }

    [ServerRpc]
    private void DestroyBulletServerRpc()
    {
        DestroyBulletClientRpc();
    }

    [ClientRpc]
    private void DestroyBulletClientRpc()
    {
        Destroy(gameObject);
    }

    void OnDrawGizmosSelected()
    {
        if (Explosive)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, explosionArea);
        }
    }
}
