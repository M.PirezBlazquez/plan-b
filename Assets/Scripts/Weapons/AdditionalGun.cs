using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalGun : MonoBehaviour
{
    public Transform[] firePoints;
    public GameObject bulletPrefab;

    [Header("AMMO")]
    public int maxAmmo = 30;
    public int magazineSize = 10;
    public int currentAmmo;
    public int currentMagazine;

    [Header("Fire Rate")]
    public float timeBetweenShots = 0.5f;
    public float nextTimeToFire = 0f;

    [Header("Reload")]
    public float reloadTime = 2f;
    public bool isReloading = false;

    [Header("Deviation")]
    public float deviationAngle = 15f; // en grados

    [Header("Charged Shot")]
    public bool isChargeWeapon = false;
    public bool chargedShot = false;
    public float chargeTime = 0.5f;
    [SerializeField] private float chargeTimer = 0f;

    [Header("Visual")]
    public Sprite spriteArmaUI;

    [Header("Animation")]
    public Animator animator; 

    void Start()
    {
        currentAmmo = maxAmmo;
        currentMagazine = magazineSize;

        UpdateIdleAnimation();
    }

    void Update()
    {
        if (isReloading)
            return;

        if (currentMagazine <= 0 && currentAmmo > 0)
        {
            StartCoroutine(Reload());
            return;
        }

        if (isChargeWeapon)
        {
            HandleChargeWeapon();
        }
        else
        {
            HandleNormalWeapon();
        }

        if (Input.GetKeyDown(KeyCode.R) && currentMagazine < magazineSize && currentAmmo > 0 && currentMagazine != currentAmmo)
        {
            StartCoroutine(Reload());
        }
    }

    void HandleNormalWeapon()
    {
        if (Input.GetMouseButton(0) && Time.time >= nextTimeToFire)
        {
            if (currentMagazine > 0)
            {
                Shoot();
                nextTimeToFire = Time.time + timeBetweenShots;
            }
        }
    }

    void HandleChargeWeapon()
    {
        if (Input.GetMouseButton(0))
        {
            chargeTimer += Time.deltaTime;
            if (chargeTimer >= chargeTime)
            {
                chargedShot = true;
                animator.SetBool("ChargedShotReady", true);
            }
            else
            {
                animator.SetBool("IsCharging", true);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            animator.SetBool("IsCharging", false);
            animator.SetBool("ChargedShotReady", false);
            if (chargedShot && currentMagazine > 0)
            {
                Shoot();
                nextTimeToFire = Time.time + timeBetweenShots;
            }
            chargeTimer = 0f;
            chargedShot = false;
        }
    }

    void Shoot()
    {
        animator.SetBool("IsShooting", true);
        foreach (var firePoint in firePoints)
        {
            float randomDeviation = Random.Range(-deviationAngle, deviationAngle);
            Quaternion originalRotation = firePoint.rotation;
            firePoint.Rotate(0, 0, randomDeviation);

            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            bullet.transform.Rotate(0, 0, -90);

            firePoint.rotation = originalRotation;
        }
        currentMagazine--;
        currentAmmo--;

        animator.SetBool("IsShooting", true);
        StartCoroutine(ResetShootingAnimation());

        UpdateIdleAnimation();
    }

    IEnumerator ResetShootingAnimation()
    {
        yield return null;
        animator.SetBool("IsShooting", false);
    }

    IEnumerator Reload()
    {
        if (currentAmmo > 0)
        {
            isReloading = true;
            Debug.Log("Recargando...");

            animator.SetBool("IsReloading", true);

            yield return new WaitForSeconds(reloadTime);

            int ammoNeeded = magazineSize - currentMagazine;
            int bulletsToReload = Mathf.Min(ammoNeeded, currentAmmo);

            currentMagazine += bulletsToReload;

            isReloading = false;

            animator.SetBool("IsReloading", false);

            UpdateIdleAnimation();
        }
    }

    public void RefillAMMO(int refillType)
    {
        switch (refillType)
        {
            case 1:
                currentAmmo = maxAmmo;
                break;
            case 2:
                currentAmmo = Mathf.Min(maxAmmo, currentAmmo + maxAmmo / 2);
                break;
            case 3:
                currentAmmo = Mathf.Min(maxAmmo, currentAmmo + maxAmmo / 4);
                break;
            default:
                Debug.LogWarning("Numero no reconocido");
                break;
        }

        UpdateIdleAnimation();
    }

    void UpdateIdleAnimation()
    {
        if (currentMagazine > 0)
        {
            animator.SetBool("HasAmmo", true);
        }
        else
        {
            animator.SetBool("HasAmmo", false);
        }
    }
}