using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupWeapon : MonoBehaviour
{
    private WeaponInventory weaponInventory;
    private MonoBehaviour shootingScript;
    private bool playerInRange = false;

    void Start()
    {
        weaponInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<WeaponInventory>();
        shootingScript = GetComponent<MonoBehaviour>();
        if (shootingScript != null)
        {
            shootingScript.enabled = false;
        }
    }

    void Update()
    {
        if (playerInRange && Input.GetKeyDown(KeyCode.E))
        {
            PickUpWeapon();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerInRange = true;
            Debug.Log("Player entered range");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            playerInRange = false;
            Debug.Log("Player exited range");
        }
    }

    public void PickUpWeapon()
    {
        weaponInventory.EquipWeapon(gameObject);
        transform.localScale = new Vector2(1, 1);

        if (shootingScript != null)
        {
            shootingScript.enabled = true;
        }

        Collider2D weaponCollider = GetComponent<Collider2D>();
        if (weaponCollider != null)
        {
            weaponCollider.enabled = false;
        }

        gameObject.SetActive(true);
    }
}